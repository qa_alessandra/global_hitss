class Vagas

    include RSpec::Matchers
    include Capybara::DSL

    def initialize
        @link_vagas                        = EL['link_vagas']
        @campo_palabras_clave              = EL['campo_palabras_clave']
        @botao_select_puesto               = EL['botao_select_puesto']
        @seleciona_select_pais             = EL['seleciona_select_pais']     
        @seleciona_pais                    = EL['seleciona_pais']
        @seleciona_select_categoria        = EL['seleciona_select_categoria'] 
        @seleciona_categoria               = EL['seleciona_categoria']
        @botao_buscar                      = EL['botao_buscar']
        @texto_verifica_lista              = EL['texto_verifica_lista']
        
    end


    def seleciona_vagas

        
        titulo_vaga = "Analista "
        
        find(:link, @link_vagas).click
        find(@campo_palabras_clave).set titulo_vaga
        find(@botao_select_puesto).click 
        find(@seleciona_select_pais).click
        find(@seleciona_pais).click
        find("body").click
        find(@seleciona_select_categoria).click
        binding.pry
        find(@seleciona_categoria).click
        find("body").click
        find(@botao_buscar).click
        
        sleep 5
        
    end
    
    def verifico_lista
        
        expect(page.assert_text(@texto_verifica_lista)).to eq true

        sleep 5
    end

end


   
