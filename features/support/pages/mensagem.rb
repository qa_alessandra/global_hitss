class Mensagem 

    include RSpec::Matchers
    include Capybara::DSL


    def initialize

        @imagem_informacao_covid    = EL['imagem_informacao_covid']
        @fechar_modal_covid         = EL['fechar_modal_covid']    
        @campo_nome                 = EL['campo_nome']     
        @campo_email                = EL['campo_email']  
        @campo_assunto              = EL['campo_assunto']
        @botao_enviar               = EL['botao_enviar']
        @mensagem_enviada           = EL['mensagem_enviada']
        @mensagem_atualiza_pagina   = EL['mensage_atualiza_pagina']
    end

    def covid_fechar

        modal_existe = page.has_css?(@imagem_informacao_covid , wait: 3)
        if modal_existe == true
            find(@fechar_modal_covid).click
        end
        
    end


    def formulario_contato 
     
        nome_formulario     = Faker::Beer.name.titleize
        email_formulario    = Faker::Internet.email
       
        find(@campo_nome).set nome_formulario
        find(@campo_email).set email_formulario
         
    end

    def campo_assunto

        assunto             = Faker::Lorem.paragraph

        find(@campo_assunto).set assunto
        find(@botao_enviar).click 
        sleep 5
        
    end

    def mensagem_final
        
        mensagem_atualizar = page.has_text?(@mensagem_atualiza_pagina, wait: 3)
        mensagem_existe = page.has_text?(@mensagem_enviada ,  wait: 3)
          
        
        if mensagem_existe == true || mensagem_atualizar == true
           
        else
        end  
        
        sleep 9
        
    end
    
end