Dado('que acesso a pagina principal {string}') do |global_hitss|
    url = DATA[global_hitss]    
    visit(url)
end
  
Quando('preencher os campos com {string} e {string}') do |nome, email|
    @mensagem.covid_fechar
    @mensagem.formulario_contato
end
  
Quando('escrever uma mensagem') do
    @mensagem.campo_assunto
end
  
Então('devo ver a {string}') do |mensagem|
    @mensagem.mensagem_final
end
