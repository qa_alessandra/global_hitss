
#language:pt

Funcionalidade: Acessar site de vagas da Global Hits

Contexto: Acessando pagina principal 

@vaga
Cenario: Pesquisando Vagas de Analista de Teste  

Dado que acesso a pagina principal "global_hitss"
Quando selecionar os campos especificos para pesquisar a vaga
Então devo ver uma lista com minha pesquisa 