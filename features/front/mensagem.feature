
#language:pt

Funcionalidade: Acessar site Global Hitss

Contexto: Acessando pagina principal 

@contato
Cenario: Enviando mensagem de contato 

Dado que acesso a pagina principal "global_hitss"
Quando preencher os campos com "nome" e "email"
E escrever uma mensagem 
Então devo ver a "¡Tu mensaje ha sido enviado con éxito!"